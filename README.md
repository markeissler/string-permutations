# String Permutations

For a given string (example “abc”), print out every unique lower and upper 
case permutation. So, one permutation will obviously be “abc”, another “aBc”, 
another “aBC”, etc.
