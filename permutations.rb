#!ruby
#
# For a given string (example “abc”), print out every unique lower and upper 
# case permutation. So, one permutation will obviously be “abc”, another “aBc”, 
# another “aBC”, etc.
#

STRING="abc"
STRING_LEN=STRING.length

#
# There are (2^3)-1 combinations, or (2^strlen) - 1. That's 2 choices (upper and
# lowercase) for a 3-character string.
#
# We can use bitwise operations to implement flags on each character, indicating
# which ones should be capitalized.
# 
# For the string "abc" we have 8 combinations...
# 
# 0 = 000 => abc
# 1 = 001 => abC
# 2 = 010 ...
# 3 = 011
# 4 = 100 
# 5 = 101
# 6 = 110 ...
# 7 = 111 => ABC
# 
# As we iterate, over each character, we calculate a bitmask and then apply the
# bitwise & operation to determine if the character is uppercase.
# 
# For the first combination 000 (result "abc")...
# 
# pos 1 "a": 001 & 000 = 000
# pos 2 "b": 010 & 000 = 000
# pos 3 "c": 100 & 000 = 000
# 
# For the fifth combination 101 (result "AbC")...
# pos 1 "a": 001 & 101 = 001
# pos 2 "b": 010 & 101 = 000
# pos 3 "c": 100 & 101 = 100
#

combos_max = (2**STRING_LEN)

for combo in 0..(combos_max-1) do
  str_new = ""

  for c in 0..STRING_LEN-1 do
    bitmask = (2**c)
    unless (bitmask & combo) == bitmask
      str_new << STRING[c].to_s.downcase
    else
      str_new << STRING[c].to_s.upcase
    end
  end

  printf("%s\n", str_new)
end
